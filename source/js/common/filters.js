angular.module("TestACS")
    // Фильтр предназначен для того, что бы превращать полное ФИО в Фамилию + Инициалы
    .filter("tinyname", function () {
        return function (value) {
            // проверка переменной value на наличие строки
            if (angular.isString(value)) {
                var nameArray = value.split(" ");
                // Если не 3 слова в имени, то возвращаем как есть
                if (nameArray.length != 3) {
                    return value;
                } else {
                    return nameArray[0] + ' ' + nameArray[1][0] + '.' + nameArray[2][0] + '.'
                }
            } else {
                return value;
            }
        };
    });
