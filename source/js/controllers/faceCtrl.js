angular.module('TestACS')
    .controller('faceCtrl',function faceCtrl($scope, $routeParams, store) {
        // Устанавливаем шаблон основной области. Здесь у нас будет форма
        $scope.setTemplate = function() {
            return 'face.html';
        };

        // В навигатор добавляем первый элемент лица
        $scope.navigator = [
            {
                url: '#/faces',
                title: 'Лица',
            }
        ]

        // Идем по массиву с нашими данными. Добавляем данные для области навигатора
        // Если id из URL совпадает с id на текущей итерации считаем элемент активным
        // и кладем весь элемент в модель.
        var faceId = $routeParams.faceId;
        for (face in store.data) {
            var newNavigItem = {};
            if (store.data[face].id == faceId) {
                $scope.model = store.data[face];

                newNavigItem.class = 'active';
            }
            newNavigItem.url = '#/faces/' +  store.data[face].id;
            newNavigItem.title = store.data[face].name;

            $scope.navigator.push(newNavigItem);
        }




    });
