angular.module('TestACS')
    .controller('faceListCtrl',function faceListCtrl($scope, $location, store) {
        // Получаем список лиц из хранилища store
        $scope.faces = store.data;

        // Устанавливаем шаблон основной области. Здесь у нас будет grid
        $scope.setTemplate = function() {
            return 'faceList.html';
        };

        // Реализация функции роутинга при нажатии на строку
        $scope.linkTo = function(url) {
            $location.path( "faces/" + url );
        }

        // Данные для области навигатора
        $scope.navigator = [
            {
                url: '#/faces',
                title: 'Лица',
                class: 'active'
            }
        ]
    });

