// Базовый контроллер по сути не несет никакого функционала, так как приложение простое
// Создал его отдельно файлом, что бы подключить нужные нам контроллеры
require('./faceListCtrl');
require('./menuCtrl');
require('./faceCtrl');

angular.module('TestACS')
    .controller('baseCtrl', function ($scope) {

    });
