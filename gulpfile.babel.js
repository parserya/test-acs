import gulp from 'gulp';
import sass from 'gulp-sass';
import gutil from 'gulp-util';
import browserify from 'browserify';
import source from 'vinyl-source-stream';
import jsonTransform from 'gulp-json-transform';
import connect from 'gulp-connect';

// Сервер статики. Запускается любым таском
gulp.task('connect', function() {
    connect.server({
        port: 8888
    });
});

// Сборка всего JS в bundle
gulp.task('browserify', () => {
    return browserify({
        entries: 'source/js/App.js',
        debug: false
    })
        .bundle()
        .on('error', function(err){
            gutil.log(gutil.colors.red.bold('[browserify error]'));
            gutil.log(err.message);
            this.emit('end');
        })
        .pipe(source('bundle.js'))
        .pipe(connect.reload())
        .pipe(gulp.dest('./public'));
});

// Изначально использовал JSONP для работы с локальным файлом.
// Эта задача преобразовывала в JSON в JSONP. Сейчас просто перекладывает в каталог public
gulp.task('json', () => {
    gulp.src('./source/**/*.json')
        .pipe(jsonTransform(function(data, file) {
            return JSON.stringify(data);
        }))
        .pipe(gulp.dest('./public'));
});

// Сборка sass
gulp.task('sass', () => {
    gulp.src('./source/sass/styles.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./public/css'))
        .on('error', function(err) {
            // Handle errors, but do not stop watch task
            gutil.log(gutil.colors.red.bold('[Sass error]'));
            gutil.log(gutil.colors.bgRed('filename:') +' '+ err.filename);
            gutil.log(gutil.colors.bgRed('lineNumber:') +' '+ err.lineNumber);
            gutil.log(gutil.colors.bgRed('extract:') +' '+ err.extract.join(' '));
            this.emit('end');
        });
});

gulp.task('watch', () => {
    connect.server({
        port: 8888
    });
    gulp.watch('./source/js/**/*.js', ['browserify']);
    gulp.watch('./source/**/*.json', ['json']);
    gulp.watch('./source/sass/*.scss', ['sass']);
});

gulp.task('build', ['connect', 'browserify', 'json', 'sass']);
gulp.task('default', ['build', 'watch']);
