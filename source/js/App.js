var angular = require('angular'),
    angularRoute = require('angular-route');

var app = angular.module("TestACS",[angularRoute]);

// Сервис с фабрикой для получения данных из JSON
require('./services/dataStorage');

// Файл фильтров
require('./common/filters');

// Базовый контроллер
require('./controllers/baseCtrl');

// Конфигурируем роутинг
app.config(function($routeProvider) {
    // Функция получения данных из фабрики. Будет вызываться в методе resolve и складывать данные в хранилище store
    var getDataStorage = function (dataStorage) {
        return dataStorage.getData();
    };

    $routeProvider
        .when('/', {
            controller: 'menuCtrl',
            templateUrl: 'testacs-index.html',
            resolve: {
                store: getDataStorage
            }
        })
        .when('/faces', {
            controller: 'faceListCtrl',
            templateUrl: 'testacs-index.html',
            resolve: {
                store: getDataStorage
            }
        })
        .when('/faces/:faceId', {
            controller: 'faceCtrl',
            templateUrl: 'testacs-index.html',
            resolve: {
                store: getDataStorage
            }
        })
        // Если нет соответствия URL - редиректим на главную
        .otherwise({
            redirectTo: '/'
        });
});
