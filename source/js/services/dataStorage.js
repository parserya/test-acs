angular.module('TestACS')
    .factory('dataStorage',function($http) {
        return {
            getData: function() {
                // Локальный URL требует запуска сервера статики
                //var url = 'public/data.json';

                // Внешний JSON-файл
                var url = 'https://bitbucket.org/parserya/test-acs/raw/4fcecb9ae7e2a897fc1020c540802210c0f08c5a/public/data.json';
                return $http.get(url);
            }
        };

    });


